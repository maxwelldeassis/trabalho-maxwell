package bean;

import java.util.Set;

import javax.faces.bean.*;
import javax.persistence.*;

@ManagedBean(name="cursoBean", eager=true)
@SessionScoped
@Entity(name="curso")
@Table(name="curso")
public class CursoBean {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idCurso;
	private String nomeCurso;
	private int duracaoCurso;
	private Set<EmpresaBean> empresasCurso; 

	public int getId() {
		return idCurso;
	}

	public void setId(int idCurso) {
		this.idCurso = idCurso;
	}

	public String getNome() {
		return nomeCurso;
	}

	public void setNome(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}

	public int getDuracaoCurso() {
		return duracaoCurso;
	}

	public void setDuracao(int duracaoCurso) {
		this.duracaoCurso = duracaoCurso;
	}

	@ManyToMany
	@JoinTable(name="curso_empresa", joinColumns=@JoinColumn(name="idCurso"), inverseJoinColumns=@JoinColumn(name="idEmpresa"))
	public Set<EmpresaBean> getEmpresas() {
		return empresasCurso;
	}

	public void setEmpresas(Set<EmpresaBean> empresasCurso) {
		this.empresasCurso = empresasCurso;
	}
	
	public void addEmpresa(EmpresaBean empresa) {
		this.empresasCurso.add(empresa);
	}

	
}