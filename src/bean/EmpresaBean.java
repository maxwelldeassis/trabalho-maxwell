package bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.*;

@ManagedBean(name = "empresaBean", eager = true)
@SessionScoped
@Entity(name = "empresa")
public class EmpresaBean {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idEmpresa;
	private String nomeEmpresa;
	private String siteEmpresa;

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getSiteEmpresa() {
		return siteEmpresa;
	}

	public void setSiteEmpresa(String siteEmpresa) {
		this.siteEmpresa = siteEmpresa;
	}
}