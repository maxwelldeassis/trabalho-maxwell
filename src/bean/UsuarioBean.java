package bean;

import javax.faces.bean.*;
import javax.persistence.*;

@ManagedBean(name = "usuarioBean", eager = true) // ManagedBean refere-se ao
													// mapeamento do hibernate
@SessionScoped // Escopo da entidade
@Entity(name = "usuario") // Entity refere-se ao nome para ser utilizado na HQL
@Table(name = "usuario") // Table se refere ao nome da tabela no DB
public class UsuarioBean {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idUsuario;
	private String nomeUsuario;
	private String nomeCompletoUsuario;
	private String senhaUsuario;
	private int tipoUsuario;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getNomeCompletoUsuario() {
		return nomeCompletoUsuario;
	}

	public void setNomeCompletoUsuario(String nomeCompletoUsuario) {
		this.nomeCompletoUsuario = nomeCompletoUsuario;
	}

	public String getSenhaUsuario() {
		return senhaUsuario;
	}

	public void setSenhaUsuario(String senhaUsuario) {
		this.senhaUsuario = senhaUsuario;
	}

	public int getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(int tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
}